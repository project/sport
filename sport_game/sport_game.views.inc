<?php

/**
 * @file
 * TODO: description
 */

/**
 * Implementation of hook_views_plugins().
 */
function sport_game_views_plugins() {
  return array(
    'row' => array(
      'sport_game_latest' => array(
        'title' => t('Sport game latest'),
        'help' => t('...'),
        'handler' => 'views_plugin_row_sport_game_latest',
        'path' => drupal_get_path('module', 'sport_game') . '/plugins',
        'base' => array('node'),
        'type' => 'normal',
      ),
      'sport_game_upcoming' => array(
        'title' => t('Sport game upcoming'),
        'help' => t('...'),
        'handler' => 'views_plugin_row_sport_game_upcoming',
        'path' => drupal_get_path('module', 'sport_game') . '/plugins',
        'base' => array('node'),
        'type' => 'normal',
      ),
    ),
  );
}
