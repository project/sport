<?php
/**
 * @file
 * TODO
 */


/**
 * Views plugin to render a latest game entry.
 */
class views_plugin_row_sport_game_latest extends views_plugin_row {
  function render($row) {
    $node = node_load($row->nid);
    return theme('sport_game_latest_entry', $node);
  }
}
