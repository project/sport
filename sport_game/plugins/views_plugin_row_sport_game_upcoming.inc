<?php
/**
 * @file
 * TODO
 */


/**
 * Views plugin to render an upcoming game entry.
 */
class views_plugin_row_sport_game_upcoming extends views_plugin_row {
  function render($row) {
    $node = node_load($row->nid);
    return theme('sport_game_upcoming_entry', $node);
  }
}
