<?php

/**
 * @file
 * TODO: description
 */

// Imagecache presets

define(PRESET_TEAM_PICTURE_SMALL, 'team-picture-small');
define(PRESET_TEAM_PICTURE_MEDIUM, 'team-picture-medium');
define(PRESET_TEAM_PICTURE_LARGE, 'team-picture-large');

define(PRESET_TEAM_LOGO_SMALL, 'team-logo-small');
define(PRESET_TEAM_LOGO_MEDIUM, 'team-logo-medium');
define(PRESET_TEAM_LOGO_LARGE, 'team-logo-large');


/**
 * Implementation of hook_init().
 */
function sport_team_init() {
	drupal_add_css(drupal_get_path('module', 'sport_team') . '/sport_team.css');
}

/**
 * Implementation of hook_views_api().
 */
function sport_team_views_api() {
  return array('api' => 2.0);
}


/**
 * Implementation of hook_token_list().
 */
function sport_team_token_list($type = 'all') {
  if ($type == 'node' || $type == 'all') {
    $tokens['node']['sport_team_season_path'] = t("Path of the season associated with the team");
    return $tokens;
  }
}

/**
 * Implementation of hook_token_values().
 */
function sport_team_token_values($type, $object = NULL, $options = array()) {
  if ($type == 'node') {
    $node = $object;
    // TODO: make it in one sql query
    $season = node_load($node->field_team_in_season_season[0]['nid']);
    $season_path = $season->field_sport_season_path[0]['value'];
    $tokens['sport_team_season_path'] = $season_path;
    return $tokens;
  }
}




function sport_team_rebuild_menu() {
  $team_mlid = db_result(db_query("SELECT mlid FROM {menu_links} WHERE link_path='team'"));
  $db_result = db_query("SELECT mlid FROM {menu_links} WHERE plid=%d", $team_mlid);
  while ($row = db_fetch_object($db_result)) {
  	menu_link_delete($row->mlid);
  }
  $i = 0;
  foreach(sport_team_active_teams() as $team) {
    $item = array(
      'link_title' => $team->title,
      'link_path' => 'node/' . $team->nid,
      'menu_name' => 'primary-links',
      'plid' => $team_mlid,
      'weight' => $i,
    );
    menu_link_save($item);
    $i++;
  }
  db_query("UPDATE {menu_links} SET expanded=1 WHERE mlid=%d", $team_mlid);
}










function sport_team_active_teams() {
	$result = array();
	$season_nid = sport_season_active_season_nid();
	$db_result = db_query("SELECT n.nid, nt.title, t.nid AS team_nid 
	 FROM {node} n 
	 JOIN {content_type_sport_team_in_season} s ON s.vid=n.vid
	 JOIN {node} nt ON s.field_team_in_season_team_nid=nt.nid
	 JOIN {content_type_sport_team} t ON t.vid=nt.vid
	 WHERE n.type='sport_team_in_season' AND s.field_team_in_season_season_nid=%d
	 ORDER BY t.field_sport_team_weight_value ASC", $season_nid);
	while ($row = db_fetch_object($db_result)) {
		$result[] = $row;
	}
	return $result;
}



// ------------------------------------------------------------------
// Field access
// ------------------------------------------------------------------

/**
 * Node id of logo.
 */
function sport_team_logo_nid($node) {
  return $node->field_sport_team_logo[0]['nid'];
}

function sport_team_in_season_team_nid(&$node) {
	return $node->field_team_in_season_team[0]['nid'];
}

function sport_team_in_season_season_nid(&$node) {
  return $node->field_team_in_season_season[0]['nid'];
}

function sport_team_short_name(&$node) {
	return $node->field_sport_short_name[0]['value'];
}


// ------------------------------------------------------------------
// Node
// ------------------------------------------------------------------

/**
 * Implementation of hook_form_FORM_ID_alter().
 */
function sport_team_form_sport_team_in_season_node_form_alter(&$form, $form_state) {
  // Remove the title field
  $form['title']['#type'] = 'value';
  $form['title']['#value'] = $form['title']['#default_value'];
  $form['title']['#required'] = FALSE;
  // Remove short name field
  $form['field_sport_short_name'][0]['#type'] = 'value';
  $form['field_sport_short_name'][0]['#value'] = $form['field_sport_short_name'][0]['#default_value'];
  $form['field_sport_short_name'][0]['#required'] = FALSE;
  // Add additional validator
  $form['#validate'][] = '_sport_team_in_season_validate';
}


/**
 * Validation callback for person node form.
 */
function _sport_team_in_season_validate($form, &$form_state) {
  $nid = $form_state['values']['nid'];
  $team_nid = $form_state['values']['field_team_in_season_team'][0]['nid'];
  $season_nid = $form_state['values']['field_team_in_season_season'][0]['nid'];
  
  $other_nid = db_result(db_query("SELECT n.nid FROM {node} n JOIN {content_type_sport_team_in_season} t ON n.vid=t.vid WHERE t.field_team_in_season_team_nid=%d AND t.field_team_in_season_season_nid=%d", $team_nid, $season_nid));
  if ($other_nid && ($nid != $other_nid)) {
    $team = node_load($team_nid);
    $season = node_load($season_nid);
    form_set_error('title', t('The team <a href="@team-url">%team</a> has already been created in season <a href="@season-url">%season</a> (See <a href="@other-url">here</a>)',
    array(
      '@team-url' => url('node/' . $team_nid),
      '%team' => $team->title,
      '@season-url' => url('node/' . $season_nid),
      '%season' => $season->title,
      '@other-url' => url('node/' . $other_nid),
    )));
  }
}


/**
 * Implementation of hook_nodeapi().
 */
function sport_team_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'presave':
      if ($node->type == 'sport_team') {
        _sport_team_sport_team_presave($node);
      }
      if ($node->type == 'sport_team_in_season') {
        _sport_team_sport_team_in_season_presave($node);
      }
      break;

    case 'view':
    	if ($node->type == 'sport_season') {
    		_sport_team_sport_season_view($node, $a3, $a4);
    	}
    	elseif ($node->type == 'sport_team_in_season') {
    	  _sport_team_sport_team_in_season_view($node, $a3, $a4);
    	}
    	elseif ($node->type == 'club_person') {
    	  _sport_team_club_person_view($node, $a3, $a4);
    	}
    	break;

    case 'update':
    case 'insert':
    	if ($node->type == 'sport_team_in_season') {
        sport_team_rebuild_menu();
    	}
      break;
  }
}

/**
 * Presave operation for nodes of type 'sport_team'.
 */
function _sport_team_sport_team_presave(&$node) {
  // If the short name is not set, use the title as short name.
  if (empty($node->field_sport_short_name[0]['value'])) {
    $node->field_sport_short_name[0]['value'] = $node->title;
    drupal_set_message(t('The name <em>@name</em> was inserted as the team\'s short name', array('@name' => $node->title)));
  }
}

/**
 * Presave operation for nodes of type 'sport_team_in_season'.
 */
function _sport_team_sport_team_in_season_presave(&$node) {
  // Set title and short name
  $team = node_load(sport_team_in_season_team_nid($node));
  $season = node_load(sport_team_in_season_season_nid($node)); 
  $node->title = $team->title . ' - ' . $season->title;
  $node->field_sport_short_name[0]['value'] = sport_team_short_name($team);
  // Order team members alphabetically
  usort($node->field_team_in_season_player, '_sport_team_member_comparison_first_name');
  
  sport_team_rebuild_menu();
}

/**
 * Comparison function which orders by last name.
 */
function _sport_team_member_comparison_last_name($a, $b) {
  $node_a = node_load($a['person_nid']);
  $node_b = node_load($b['person_nid']);

  if (person_last_name($node_a) == person_last_name($node_b)) {
    return (person_first_name($node_a) < person_first_name($node_b)) ? -1 : 1;
  }
  else {
    return (person_last_name($node_a) < person_last_name($node_b)) ? -1 : 1;
  }
 
  return 0;
}

/**
 * Comparison function which orders by first name.
 */
function _sport_team_member_comparison_first_name($a, $b) {
  $node_a = node_load($a['person_nid']);
  $node_b = node_load($b['person_nid']);

  if ($node_a->title == $node_b->title) {
    return 0;
  }
  else {
    return ($node_a->title < $node_b->title) ? -1 : 1;
  }
 
  return 0;
}

/**
 * View operation for nodes of type 'sport_season'.
 */
function _sport_team_sport_season_view(&$node, $teaser, $page) {
	if ($page) {
	  $view = views_get_view('sport_team_team_per_season');
	  $content = '<div class="season-team-list">';
	  $content .= $view->execute_display('default', array($node->nid));
	  $content .= '</div>';
	  $content .= '<div class="clear"></div>';
	  $node->content['team_per_season'] = array(
	    '#value' => $content,
	    '#weight' => 5,
	  );
	}
}

/**
 * View operation for nodes of type 'sport_team_in_season'.
 */
function _sport_team_sport_team_in_season_view(&$node, $teaser, $page) {
	if ($page) {
    $season = node_load(sport_team_in_season_season_nid($node));

    // Breadcrumbs
	  if (sport_team_in_season_season_nid($node) == sport_season_active_season_nid()) {
	    // Active season
      $breadcrumb = array();
      $breadcrumb[] = l(t('Home'), NULL);
      $breadcrumb[] = l(t('Teams'), 'team');
      drupal_set_breadcrumb($breadcrumb);
	  }
	  else {
	    // Archive season
      $breadcrumb = array();
      $breadcrumb[] = l(t('Home'), NULL);
      $breadcrumb[] = l(t('Archive'), 'archive');
      $breadcrumb[] = l($season->title, 'node/' . $season->nid);
      drupal_set_breadcrumb($breadcrumb);
	  }
	  
	  // Season links
	  // TODO: filter by sport type
	  $db_result = db_query("SELECT n.nid, n.title FROM {node} n JOIN {content_type_sport_season} s ON n.vid=s.vid WHERE n.type='sport_season' ORDER BY s.field_sport_season_date_value2 ASC");
	  
	  $db_result = db_query("SELECT ns.nid, ns.title, n.nid AS team_nid
	    FROM {node} n
	    JOIN {content_type_sport_team_in_season} ts ON n.vid=ts.vid
	    JOIN {node} ns ON ts.field_team_in_season_season_nid=ns.nid
	    JOIN {content_type_sport_season} s ON ns.vid=s.vid
	    WHERE ts.field_team_in_season_team_nid=%d
	    ORDER BY s.field_sport_season_date_value2 ASC", sport_team_in_season_team_nid($node));
	  
	  
	  $last = NULL;
	  while ($row = db_fetch_object($db_result)) {
	    if ($row->nid == $season->nid) {
	      break;
	    }
	    $last = $row;
	  }
	  $next = db_fetch_object($db_result);
	  
	  $links = '';
	  if ($last) {
	    $links .= '<div class="team-links-left">' . l('<< ' . $last->title, 'node/' . $last->team_nid) . '</div>';
	  }
	  if ($next) {
	    $links .= '<div class="team-links-right">' . l($next->title . ' >>', 'node/' . $next->team_nid) . '</div>';
	  }
	  $links .= '<div class="team-links-center">' . l($season->title, 'node/' . $season->nid) . '</div>';
	  if ($links) {
	    $links .= '<div style="clear:both"></div>';
	  }
	    
	  $node->content['season_links'] = array(
	    '#value' => $links,
	    '#weight' => -20,
	  );
	}
}

/**
 * View operation for nodes of type 'club_person'.
 */
function _sport_team_club_person_view(&$node, $teaser, $page) {
  if ($page) {
    $content = '<div class="sport-team-player-history">';
    $content .= '<h2>' . t('Player history') . '</h2>';
    $content .= views_embed_view('sport_team_teams_per_player', 'default', $node->nid);
    $content .= '</div>';
    $node->content['sport_team_player_history'] = array(
      '#value' => $content,
      '#weight' => 10,
    );
    
    $content = '';
    $content = '<div class="sport-team-staff-history">';
    $content .= '<h2>' . t('Staff history') . '</h2>';
    $content .= views_embed_view('sport_team_teams_per_staff', 'default', $node->nid);
    $content .= '</div>';
    $node->content['sport_team_staff_history'] = array(
      '#value' => $content,
      '#weight' => 20,
    );
  }
}













/**
 * Implementation of hook_theme().
 */
function sport_team_theme($existing) {
  return array(
    'views_view_unformatted__sport_team_team_per_season' => array (
      'arguments' => array('view' => NULL, 'options' => NULL, 'rows' => NULL, 'title' => NULL),
      'template' => 'views-view-unformatted--sport-team-team-per-season',
      'path' => drupal_get_path('module', 'sport_team') . '/templates',
      'original hook' => 'views_view_unformatted',
    ),
    'views_view_fields__sport_team_team_per_season' => array (
      'arguments' => array('view' => NULL, 'options' => NULL, 'row' => NULL),
      'template' => 'views-view-fields--sport-team-team-per-season',
      'path' => drupal_get_path('module', 'sport_team') . '/templates',
      'original hook' => 'views_view_fields',
    ),
  );
}


/**
 * Implementation of hook_form_FORM_ID_alter().
 */
function sport_team_form_sport_core_settings_form_alter(&$form, $form_state) {
  $form['sport_team_display'] = array(
    '#type' => 'fieldset',
    '#title' => 'Team display settings',
    '#weight' => -1,
  );
  $form['sport_team_display']['sport_team_player_layout'] = array(
    '#type' => 'select',
    '#title' => t('Player listing layout'),
    '#default_value' => sport_team_player_layout(),
    '#options' => array(
      'sport_team_member_two_column' => t('Two columns / Medium pictures (default)'),
      'sport_team_member_table' => t('Table layout / Small pictures'),
    ),
  );
  $form['sport_team_display']['sport_team_player_order'] = array(
    '#type' => 'select',
    '#title' => t('Player listing order'),
    '#default_value' => sport_team_player_order(),
    '#options' => array(
      '_sport_team_member_comparison_first_name' => t('By first name (default)'),
      '_sport_team_member_comparison_last_name' => t('By last name'),
    ),
  );
}


function sport_team_player_layout() {
	return variable_get('sport_team_player_layout', 'sport_team_member_two_column');
}

function sport_team_player_order() {
  return variable_get('sport_team_player_order', '_sport_team_member_comparison_first_name');
}
