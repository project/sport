<?php

/**
 * @file
 * TODO: description
 */

/**
 * Implementation of hook_views_default_views().
 */
function sport_team_views_default_views() {
  /*
   * View 'sport_team_admin_list'
   */
  $view = new view;
  $view->name = 'sport_team_admin_list';
  $view->description = 'List of teams.';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'edit_node' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'text' => '',
      'exclude' => 0,
      'id' => 'edit_node',
      'table' => 'node',
      'field' => 'edit_node',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
    'title' => array(
      'label' => 'Title',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'none',
    ),
    'field_sport_short_name_value' => array(
      'label' => 'Short name',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_sport_short_name_value',
      'table' => 'node_data_field_sport_short_name',
      'field' => 'field_sport_short_name_value',
      'relationship' => 'none',
    ),
    'field_sport_team_logo_nid' => array(
      'label' => 'Team logo',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'imagecache__team-logo-small',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_sport_team_logo_nid',
      'table' => 'node_data_field_sport_team_logo',
      'field' => 'field_sport_team_logo_nid',
      'relationship' => 'none',
    ),
    'field_sport_team_weight_value' => array(
      'label' => 'Weight',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_sport_team_weight_value',
      'table' => 'node_data_field_sport_team_weight',
      'field' => 'field_sport_team_weight_value',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'field_sport_team_weight_value' => array(
      'id' => 'field_sport_team_weight_value',
      'table' => 'node_data_field_sport_team_weight',
      'field' => 'field_sport_team_weight_value',
    ),
  ));
  $handler->override_option('filters', array(
    'status_extra' => array(
      'id' => 'status_extra',
      'table' => 'node',
      'field' => 'status_extra',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'sport_team' => 'sport_team',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Teams');
  $handler->override_option('items_per_page', 0);
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'edit_node' => 'edit_node',
      'title' => 'title',
      'field_sport_team_logo_fid' => 'field_sport_team_logo_fid',
    ),
    'info' => array(
      'edit_node' => array(
        'separator' => '',
      ),
      'title' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_sport_team_logo_fid' => array(
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'admin/sport/team/list');
  $handler->override_option('menu', array(
    'type' => 'default tab',
    'title' => 'Teams',
    'description' => 'List of teams.',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'normal',
    'title' => 'Teams',
    'description' => 'List of teams.',
    'weight' => '0',
  ));
  $views[$view->name] = $view;

  /*
   * View 'sport_team_admin_list_season'
   */
  $view = new view;
  $view->name = 'sport_team_admin_list_season';
  $view->description = 'List of teams per season.';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'field_team_in_season_team_nid' => array(
      'label' => 'Team',
      'required' => 1,
      'delta' => -1,
      'id' => 'field_team_in_season_team_nid',
      'table' => 'node_data_field_team_in_season_team',
      'field' => 'field_team_in_season_team_nid',
      'relationship' => 'none',
    ),
    'field_team_in_season_season_nid' => array(
      'label' => 'Season',
      'required' => 1,
      'delta' => -1,
      'id' => 'field_team_in_season_season_nid',
      'table' => 'node_data_field_team_in_season_season',
      'field' => 'field_team_in_season_season_nid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('fields', array(
    'nid' => array(
      'label' => 'Nid',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'exclude' => 1,
      'id' => 'nid',
      'table' => 'node',
      'field' => 'nid',
      'relationship' => 'none',
    ),
    'edit_node' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'text' => '',
      'exclude' => 0,
      'id' => 'edit_node',
      'table' => 'node',
      'field' => 'edit_node',
      'relationship' => 'none',
    ),
    'title' => array(
      'label' => 'Title',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 1,
        'path' => 'node/[nid]',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'field_team_in_season_team_nid',
    ),
    'field_sport_short_name_value' => array(
      'label' => 'Short name',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_sport_short_name_value',
      'table' => 'node_data_field_sport_short_name',
      'field' => 'field_sport_short_name_value',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'field_team_in_season_picture_nid' => array(
      'label' => 'Picture',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'imagecache_link_image__image-tiny',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_team_in_season_picture_nid',
      'table' => 'node_data_field_team_in_season_picture',
      'field' => 'field_team_in_season_picture_nid',
      'relationship' => 'none',
    ),
    'title_1' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'exclude' => 1,
      'id' => 'title_1',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'field_team_in_season_season_nid',
    ),
  ));
  $handler->override_option('sorts', array(
    'field_sport_season_date_value2' => array(
      'order' => 'DESC',
      'delta' => -1,
      'id' => 'field_sport_season_date_value2',
      'table' => 'node_data_field_sport_season_date',
      'field' => 'field_sport_season_date_value2',
      'relationship' => 'field_team_in_season_season_nid',
    ),
    'field_sport_team_weight_value' => array(
      'order' => 'ASC',
      'delta' => -1,
      'id' => 'field_sport_team_weight_value',
      'table' => 'node_data_field_sport_team_weight',
      'field' => 'field_sport_team_weight_value',
      'relationship' => 'field_team_in_season_team_nid',
    ),
  ));
  $handler->override_option('filters', array(
    'status_extra' => array(
      'id' => 'status_extra',
      'table' => 'node',
      'field' => 'status_extra',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'sport_team_in_season' => 'sport_team_in_season',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('items_per_page', 0);
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => 'title_1',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'edit_node' => 'edit_node',
      'title' => 'title',
      'title_1' => 'title_1',
    ),
    'info' => array(
      'edit_node' => array(
        'separator' => '',
      ),
      'title' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'title_1' => array(
        'sortable' => 0,
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'admin/sport/team/season');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'Per season',
    'description' => 'List of teams per season.',
    'weight' => '4',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  $views[$view->name] = $view;

  /*
   * View 'sport_team_option_list'
   */
  $view = new view;
  $view->name = 'sport_team_option_list';
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'title' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'field_sport_team_weight_value' => array(
      'order' => 'ASC',
      'delta' => -1,
      'id' => 'field_sport_team_weight_value',
      'table' => 'node_data_field_sport_team_weight',
      'field' => 'field_sport_team_weight_value',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'status' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'sport_team' => 'sport_team',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('items_per_page', 0);
  $views[$view->name] = $view;

  /*
   * View 'sport_team_team_per_season'
   */
  $view = new view;
  $view->name = 'sport_team_team_per_season';
  $view->description = 'Public list of teams active in a season.';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'field_team_in_season_season_nid' => array(
      'label' => 'Season',
      'required' => 1,
      'delta' => -1,
      'id' => 'field_team_in_season_season_nid',
      'table' => 'node_data_field_team_in_season_season',
      'field' => 'field_team_in_season_season_nid',
      'relationship' => 'none',
    ),
    'field_team_in_season_team_nid' => array(
      'label' => 'Team',
      'required' => 1,
      'delta' => -1,
      'id' => 'field_team_in_season_team_nid',
      'table' => 'node_data_field_team_in_season_team',
      'field' => 'field_team_in_season_team_nid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('fields', array(
    'nid' => array(
      'label' => 'Nid',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'exclude' => 1,
      'id' => 'nid',
      'table' => 'node',
      'field' => 'nid',
      'relationship' => 'none',
    ),
    'title' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 1,
        'path' => 'node/[nid]',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'field_team_in_season_team_nid',
    ),
    'field_team_in_season_picture_nid' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'none',
      'format' => 'imagecache_link_node__team-picture-small',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_team_in_season_picture_nid',
      'table' => 'node_data_field_team_in_season_picture',
      'field' => 'field_team_in_season_picture_nid',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'field_sport_team_weight_value' => array(
      'order' => 'ASC',
      'delta' => -1,
      'id' => 'field_sport_team_weight_value',
      'table' => 'node_data_field_sport_team_weight',
      'field' => 'field_sport_team_weight_value',
      'relationship' => 'field_team_in_season_team_nid',
    ),
  ));
  $handler->override_option('arguments', array(
    'field_team_in_season_season_nid' => array(
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'php',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'field_team_in_season_season_nid',
      'table' => 'node_data_field_team_in_season_season',
      'field' => 'field_team_in_season_season_nid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
        '4' => 0,
        '3' => 0,
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => 'return sport_season_active_season_nid();',
      'validate_argument_node_type' => array(
        'webform' => 0,
        'panel' => 0,
        'article' => 0,
        'board' => 0,
        'event' => 0,
        'gallery' => 0,
        'page' => 0,
        'person' => 0,
        'sport_game' => 0,
        'sport_league' => 0,
        'sport_location' => 0,
        'sport_opponent' => 0,
        'sport_season' => 0,
        'sport_team' => 0,
        'sport_team_in_season' => 0,
        'story' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_php' => '',
      'override' => array(
        'button' => 'Override',
      ),
    ),
  ));
  $handler->override_option('filters', array(
    'status' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'sport_team_in_season' => 'sport_team_in_season',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'access content',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Teams');
  $handler->override_option('items_per_page', 0);
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'team');
  $handler->override_option('menu', array(
    'type' => 'normal',
    'title' => 'Teams',
    'description' => '',
    'weight' => '-5',
    'name' => 'primary-links',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  $views[$view->name] = $view;

  /*
   * View 'sport_team_teams_per_player'
   */
  $view = new view;
  $view->name = 'sport_team_teams_per_player';
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'field_team_in_season_season_nid' => array(
      'label' => 'Season',
      'required' => 1,
      'delta' => -1,
      'id' => 'field_team_in_season_season_nid',
      'table' => 'node_data_field_team_in_season_season',
      'field' => 'field_team_in_season_season_nid',
      'relationship' => 'none',
    ),
    'field_team_in_season_team_nid' => array(
      'label' => 'Team',
      'required' => 1,
      'delta' => -1,
      'id' => 'field_team_in_season_team_nid',
      'table' => 'node_data_field_team_in_season_team',
      'field' => 'field_team_in_season_team_nid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('fields', array(
    'nid' => array(
      'label' => 'Nid',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'exclude' => 1,
      'id' => 'nid',
      'table' => 'node',
      'field' => 'nid',
      'relationship' => 'none',
    ),
    'title_1' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'exclude' => 1,
      'id' => 'title_1',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'field_team_in_season_season_nid',
    ),
    'title_2' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 1,
        'path' => 'node/[nid]',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'title_2',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'field_team_in_season_team_nid',
    ),
  ));
  $handler->override_option('sorts', array(
    'field_sport_season_date_value2' => array(
      'order' => 'DESC',
      'delta' => -1,
      'id' => 'field_sport_season_date_value2',
      'table' => 'node_data_field_sport_season_date',
      'field' => 'field_sport_season_date_value2',
      'relationship' => 'field_team_in_season_season_nid',
    ),
    'field_sport_team_weight_value' => array(
      'order' => 'ASC',
      'delta' => -1,
      'id' => 'field_sport_team_weight_value',
      'table' => 'node_data_field_sport_team_weight',
      'field' => 'field_sport_team_weight_value',
      'relationship' => 'field_team_in_season_team_nid',
    ),
  ));
  $handler->override_option('arguments', array(
    'field_team_in_season_player_person_nid' => array(
      'default_action' => 'not found',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'field_team_in_season_player_person_nid',
      'table' => 'node_data_field_team_in_season_player',
      'field' => 'field_team_in_season_player_person_nid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
        '4' => 0,
        '3' => 0,
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'webform' => 0,
        'club_article' => 0,
        'club_board' => 0,
        'club_event' => 0,
        'club_gallery' => 0,
        'club_image' => 0,
        'club_person' => 0,
        'page' => 0,
        'sport_game' => 0,
        'sport_league' => 0,
        'sport_location' => 0,
        'sport_opponent' => 0,
        'sport_season' => 0,
        'sport_team' => 0,
        'sport_team_in_season' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('style_plugin', 'list');
  $handler->override_option('style_options', array(
    'grouping' => 'title_1',
    'type' => 'ul',
  ));
  $views[$view->name] = $view;

  /*
   * View 'sport_team_teams_per_staff'
   */
  $view = new view;
  $view->name = 'sport_team_teams_per_staff';
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'field_team_in_season_season_nid' => array(
      'label' => 'Season',
      'required' => 1,
      'delta' => -1,
      'id' => 'field_team_in_season_season_nid',
      'table' => 'node_data_field_team_in_season_season',
      'field' => 'field_team_in_season_season_nid',
      'relationship' => 'none',
    ),
    'field_team_in_season_team_nid' => array(
      'label' => 'Team',
      'required' => 1,
      'delta' => -1,
      'id' => 'field_team_in_season_team_nid',
      'table' => 'node_data_field_team_in_season_team',
      'field' => 'field_team_in_season_team_nid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('fields', array(
    'nid' => array(
      'label' => 'Nid',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'exclude' => 1,
      'id' => 'nid',
      'table' => 'node',
      'field' => 'nid',
      'relationship' => 'none',
    ),
    'title_1' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'exclude' => 1,
      'id' => 'title_1',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'field_team_in_season_season_nid',
    ),
    'title_2' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 1,
        'path' => 'node/[nid]',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'title_2',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'field_team_in_season_team_nid',
    ),
    'field_team_in_season_staff_person_nid' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'none',
      'format' => 'role',
      'multiple' => array(
        'group' => 0,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => 0,
      ),
      'exclude' => 0,
      'id' => 'field_team_in_season_staff_person_nid',
      'table' => 'node_data_field_team_in_season_staff',
      'field' => 'field_team_in_season_staff_person_nid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'field_sport_season_date_value2' => array(
      'order' => 'DESC',
      'delta' => -1,
      'id' => 'field_sport_season_date_value2',
      'table' => 'node_data_field_sport_season_date',
      'field' => 'field_sport_season_date_value2',
      'relationship' => 'field_team_in_season_season_nid',
    ),
    'field_sport_team_weight_value' => array(
      'order' => 'ASC',
      'delta' => -1,
      'id' => 'field_sport_team_weight_value',
      'table' => 'node_data_field_sport_team_weight',
      'field' => 'field_sport_team_weight_value',
      'relationship' => 'field_team_in_season_team_nid',
    ),
  ));
  $handler->override_option('arguments', array(
    'field_team_in_season_staff_person_nid' => array(
      'default_action' => 'not found',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'field_team_in_season_staff_person_nid',
      'table' => 'node_data_field_team_in_season_staff',
      'field' => 'field_team_in_season_staff_person_nid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
        '4' => 0,
        '3' => 0,
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'webform' => 0,
        'club_article' => 0,
        'club_board' => 0,
        'club_event' => 0,
        'club_gallery' => 0,
        'club_image' => 0,
        'club_person' => 0,
        'page' => 0,
        'sport_game' => 0,
        'sport_league' => 0,
        'sport_location' => 0,
        'sport_opponent' => 0,
        'sport_season' => 0,
        'sport_team' => 0,
        'sport_team_in_season' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('style_plugin', 'list');
  $handler->override_option('style_options', array(
    'grouping' => 'title_1',
    'type' => 'ul',
  ));
  $handler->override_option('row_options', array(
    'inline' => array(
      'title_2' => 'title_2',
      'field_team_in_season_staff_person_nid' => 'field_team_in_season_staff_person_nid',
    ),
    'separator' => ' - ',
  ));
  $views[$view->name] = $view;

  return $views;
}


