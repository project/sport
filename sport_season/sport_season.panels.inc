$handler = new stdClass;
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'node_view_panel_context';
$handler->task = 'node_view';
$handler->subtask = '';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'autogenerate_title' => TRUE,
  'title' => 'Panel: Node being viewed can be type "Season"',
  'no_blocks' => 0,
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      '0' => array(
        'name' => 'node_type',
        'settings' => array(
          'type' => array(
            'sport_season' => 'sport_season',
          ),
        ),
        'context' => 'argument_nid_1',
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display;
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style' => 'tabs',
  'style_settings' => array(
    'default' => array(
      'filling_tabs' => 'equal',
    ),
  ),
);
$display->cache = array();
$display->title = '';
$display->hide_title = FALSE;
$display->content = array();
$display->panels = array();
  $pane = new stdClass;
  $pane->pid = 'new-1';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'view_sport_team_team_per_season';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'nodes_per_page' => '0',
    'pager_id' => '1',
    'use_pager' => 0,
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'default',
    'context' => array(
      '0' => 'argument_nid_1.nid',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $display->content['new-1'] = $pane;
  $display->panels['middle'][0] = 'new-1';
  $pane = new stdClass;
  $pane->pid = 'new-2';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'view_sport_league_per_season';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'nodes_per_page' => '0',
    'pager_id' => '1',
    'use_pager' => 0,
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'default',
    'context' => array(
      '0' => 'argument_nid_1.nid',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $display->content['new-2'] = $pane;
  $display->panels['middle'][1] = 'new-2';
  $pane = new stdClass;
  $pane->pid = 'new-3';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'view_sport_game_sched_per_season';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'nodes_per_page' => '0',
    'pager_id' => '1',
    'use_pager' => 0,
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'default',
    'context' => array(
      '0' => 'argument_nid_1.nid',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $display->content['new-3'] = $pane;
  $display->panels['middle'][2] = 'new-3';
$handler->conf['display'] = $display;
